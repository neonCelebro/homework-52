import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Lotery from './Lotery/Lotery.js';
import './btn.css'

class App extends Component {
  state = {
    value : [],
  };
  compareNumeric = (a, b) => a - b;
  addNumbersInValueCollection = () =>{
    let randomNumber = [];
    for (let i = 0; randomNumber.length <= 5; i++) {
      let number = (Math.floor(Math.random() * (36 - 5)) + 5);
      while (randomNumber.indexOf(number) >= 0) {
        number = (Math.floor(Math.random() * (36 - 5)) + 5);
      }
      this.randomNumber = randomNumber.push(number)
    };
    this.setState({
      value: randomNumber.sort(this.compareNumeric)
    })
  }
  NewNumbers = () =>{
    this.addNumbersInValueCollection();
  };
  render() {
    return (
        <div className="center-wrap">
          <button type='button' onClick={this.NewNumbers} className="plus">
            </button>
            <Lotery value={this.state.value[0]} />
            <Lotery value={this.state.value[1]} />
            <Lotery value={this.state.value[2]} />
            <Lotery value={this.state.value[3]} />
            <Lotery value={this.state.value[4]} />
          </div>
    );
  }
}

export default App;
